package com.test.poc.vehiclecontrol.service;

import java.time.LocalDateTime;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.test.poc.vehiclecontrol.domain.dto.VehicleDetailRetrievalDto;
import com.test.poc.vehiclecontrol.domain.dto.VehicleRetrievalDto;
import com.test.poc.vehiclecontrol.domain.mapper.VehicleMapper;
import com.test.poc.vehiclecontrol.domain.request.VehicleCreateRequest;
import com.test.poc.vehiclecontrol.domain.request.VehicleFilterRequest;
import com.test.poc.vehiclecontrol.domain.request.VehicleUpdatePartialRequest;
import com.test.poc.vehiclecontrol.domain.request.VehicleUpdateRequest;
import com.test.poc.vehiclecontrol.entity.Vehicle;
import com.test.poc.vehiclecontrol.repository.VehicleRepository;


@Service
@Transactional("transactionManager")
public class VehicleService {

	@Autowired
	private VehicleRepository vehicleRepository;
	
	@Autowired
	private VehicleMapper vehicleMapper;

	public List<VehicleRetrievalDto> getVehiclesAll() {
		return vehicleMapper.toVehicleRetrievalDto(vehicleRepository.findAll());
	}

	public List<VehicleRetrievalDto> getVehiclesFind(VehicleFilterRequest vehicleFilterRequest) {
		if(StringUtils.isNotBlank(vehicleFilterRequest.getBrand()) && StringUtils.isNotBlank(vehicleFilterRequest.getName())) {
			return findByNameAndBrand(vehicleFilterRequest);
		}else if(StringUtils.isNotBlank(vehicleFilterRequest.getName())) {
			return findByName(vehicleFilterRequest);
		} else if (StringUtils.isNotBlank(vehicleFilterRequest.getBrand())) {
			return findByBrand(vehicleFilterRequest);
		}
		return vehicleMapper.toVehicleRetrievalDto(vehicleRepository.findAll());
	}

	public VehicleDetailRetrievalDto getVehicle(Long vehicleId) {
		return vehicleMapper.toVehicleDetailRetrievalDto(findVehicleById(vehicleId));
	}

	public VehicleDetailRetrievalDto createVehicle(VehicleCreateRequest vehicleCreateRequest) {
		Vehicle vehicle = vehicleMapper.toEntity(vehicleCreateRequest);
		vehicle.setCreated(LocalDateTime.now());
		return vehicleMapper.toVehicleDetailRetrievalDto(vehicleRepository.save(vehicle));
	}

	public VehicleDetailRetrievalDto updateVehicle(VehicleUpdateRequest vehicleUpdateRequest) {
		Vehicle vehicle = vehicleMapper.toEntity(vehicleUpdateRequest);
		Vehicle vehicleUpdate = findVehicleById(vehicle.getId());
		vehicle.setCreated(vehicleUpdate.getCreated());
		vehicle.setUpdated(LocalDateTime.now());
		return vehicleMapper.toVehicleDetailRetrievalDto(vehicleRepository.save(vehicle));
	}

	public VehicleDetailRetrievalDto updatePartialVehicle(VehicleUpdatePartialRequest vehicleUpdatePartialRequest) {
		Vehicle vehicle = findVehicleById(vehicleUpdatePartialRequest.getId());
		Vehicle entityUpdate = vehicleMapper.toEntity(vehicleUpdatePartialRequest);
		vehicle.setBrand(StringUtils.isNoneBlank(entityUpdate.getBrand())? entityUpdate.getBrand(): vehicle.getBrand());
		vehicle.setDescription(StringUtils.isNoneBlank(entityUpdate.getDescription())? entityUpdate.getDescription(): vehicle.getDescription());
		vehicle.setName(StringUtils.isNoneBlank(entityUpdate.getName())? entityUpdate.getName(): vehicle.getName());
		vehicle.setSold(entityUpdate.isSold());
		vehicle.setYear(entityUpdate.getYear() != null ? entityUpdate.getYear(): vehicle.getYear());
		vehicle.setUpdated(LocalDateTime.now());
		Vehicle vehicleNew = vehicleRepository.save(vehicle);
		return vehicleMapper.toVehicleDetailRetrievalDto(vehicleNew);
	}

	public void deleteVechicle(Long vehicleId) {
		vehicleRepository.deleteById(vehicleId);
	}
	
	private List<VehicleRetrievalDto> findByName(VehicleFilterRequest vehicleFilterRequest) {
		return vehicleMapper.toVehicleRetrievalDto(vehicleRepository
				.findByNameContainingIgnoreCase(vehicleFilterRequest.getName()));
	}

	private List<VehicleRetrievalDto> findByBrand(VehicleFilterRequest vehicleFilterRequest) {
		return vehicleMapper.toVehicleRetrievalDto(vehicleRepository
				.findByBrandContainingIgnoreCase(vehicleFilterRequest.getBrand()));
	}
	
	private List<VehicleRetrievalDto> findByNameAndBrand(VehicleFilterRequest vehicleFilterRequest) {
		return vehicleMapper.toVehicleRetrievalDto(vehicleRepository
				.findByBrandContainingIgnoreCaseAndNameContainingIgnoreCase(vehicleFilterRequest.getBrand(), 
				vehicleFilterRequest.getName()));
	}
	
	private Vehicle findVehicleById(Long vehicleId) {
		return vehicleRepository.findById(vehicleId)
				.orElseThrow(() -> new RuntimeException(String.format("Vehicle id: [%d] não encontrado", vehicleId)));
	}
	
}
