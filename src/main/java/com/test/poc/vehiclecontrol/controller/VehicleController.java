package com.test.poc.vehiclecontrol.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.test.poc.vehiclecontrol.domain.request.VehicleCreateRequest;
import com.test.poc.vehiclecontrol.domain.request.VehicleFilterRequest;
import com.test.poc.vehiclecontrol.domain.request.VehicleUpdatePartialRequest;
import com.test.poc.vehiclecontrol.domain.request.VehicleUpdateRequest;
import com.test.poc.vehiclecontrol.domain.response.VehiClesResponse;
import com.test.poc.vehiclecontrol.domain.response.VehicleResponse;
import com.test.poc.vehiclecontrol.service.VehicleService;

import io.swagger.annotations.ApiOperation;

@RestController
@CrossOrigin
@Validated
public class VehicleController {
	
	private static final String VEHICLE = "/veiculo";
	private static final String VEHICLE_FIND = VEHICLE + "/find";
	private static final String VEHICLE_ID = VEHICLE + "/{id}";

	@Autowired
	private VehicleService vehicleService;
	
	@ApiOperation(value = "Retrieval vehicles", nickname = "getVehiclesAll")
	@GetMapping(value = VEHICLE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(code = HttpStatus.OK)
	public VehiClesResponse getVehiclesAll() {
		VehiClesResponse vehiClesResponse = new VehiClesResponse();
		vehiClesResponse.setVehicles(vehicleService.getVehiclesAll());
		return vehiClesResponse;
	}
	
	@ApiOperation(value = "Retrieval vehicles", nickname = "getVehicles")
	@GetMapping(value = VEHICLE_FIND, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(code = HttpStatus.OK)
	public VehiClesResponse getVehicles(VehicleFilterRequest vehicleFilterRequest) {
		VehiClesResponse vehiClesResponse = new VehiClesResponse();
		vehiClesResponse.setVehicles(vehicleService.getVehiclesFind(vehicleFilterRequest));
		return vehiClesResponse;
	}
	
	@ApiOperation(value = "Retrieve vehicle", nickname = "getVehicle")
	@GetMapping(value = VEHICLE_ID, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(code = HttpStatus.OK)
	public VehicleResponse getVehicle(@PathVariable("vehicleId") Long vehicleId) {
		VehicleResponse vehicleResponse = new VehicleResponse();
		vehicleResponse.setVehicle(vehicleService.getVehicle(vehicleId));
		return vehicleResponse;
	}
	
	@ApiOperation(value = "Create vehicle", nickname = "createVehicle")
	@PostMapping(value = VEHICLE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(code = HttpStatus.CREATED)
	public VehicleResponse createVehicle(@RequestBody VehicleCreateRequest vehicleCreateRequest) {
		VehicleResponse vehicleResponse = new VehicleResponse();
		vehicleResponse.setVehicle(vehicleService.createVehicle(vehicleCreateRequest));
		return vehicleResponse;
	}
	
	@ApiOperation(value = "Update vehicle", nickname = "updateVehicle")
	@PutMapping(value = VEHICLE_ID, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(code = HttpStatus.OK)
	public VehicleResponse updateVehicle(
			@PathVariable("id") Long vehicleId, 
			@RequestBody VehicleUpdateRequest vehicleUpdateRequest) {
		vehicleUpdateRequest.setId(vehicleId);
		VehicleResponse vehicleResponse = new VehicleResponse();
		vehicleResponse.setVehicle(vehicleService.updateVehicle(vehicleUpdateRequest));
		return vehicleResponse;
	}
	
	@ApiOperation(value = "Update vehicle partial", nickname = "updatePartialVehicle")
	@PatchMapping(value = VEHICLE_ID, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(code = HttpStatus.OK)
	public VehicleResponse updatePartialVehicle(
			@PathVariable("vehicleId") Long vehicleId, 
			VehicleUpdatePartialRequest vehicleUpdatePartialRequest) {
		vehicleUpdatePartialRequest.setId(vehicleId);
		VehicleResponse vehicleResponse = new VehicleResponse();
		vehicleResponse.setVehicle(vehicleService.updatePartialVehicle(vehicleUpdatePartialRequest));
		return vehicleResponse;
	}

	@ApiOperation(value = "Delete vehicle", nickname = "deleteVechicle")
	@DeleteMapping(value = VEHICLE_ID, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void deleteVechicle(@PathVariable("id") Long vehicleId) {
		vehicleService.deleteVechicle(vehicleId);
	}
	
}
