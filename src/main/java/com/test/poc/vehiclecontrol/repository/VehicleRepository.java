package com.test.poc.vehiclecontrol.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.test.poc.vehiclecontrol.entity.Vehicle;

@Repository
public interface VehicleRepository extends JpaRepository<Vehicle, Long>{

	List<Vehicle> findByBrandContainingIgnoreCaseAndNameContainingIgnoreCase(String brand, String name);

	List<Vehicle> findByNameContainingIgnoreCase(String name);
	
	List<Vehicle> findByBrandContainingIgnoreCase(String brand);

}
