package com.test.poc.vehiclecontrol.domain.response;

import java.util.List;

import com.test.poc.vehiclecontrol.domain.dto.VehicleRetrievalDto;

import io.swagger.annotations.ApiModel;

@ApiModel("Vehicles Response")
public class VehiClesResponse {

	private List<VehicleRetrievalDto> vehicles;

	public List<VehicleRetrievalDto> getVehicles() {
		return vehicles;
	}

	public void setVehicles(List<VehicleRetrievalDto> vehicles) {
		this.vehicles = vehicles;
	}
	
}
