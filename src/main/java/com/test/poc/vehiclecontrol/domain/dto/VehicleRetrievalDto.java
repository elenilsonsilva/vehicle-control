package com.test.poc.vehiclecontrol.domain.dto;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@JsonInclude(Include.NON_EMPTY)
@ApiModel(description = "Vehicle Retrieval")
public class VehicleRetrievalDto {

	@ApiModelProperty(value = "Vehicle id")
	private Long vehicleId;
		
	@ApiModelProperty(value = "Vehicle name")
	private String name;
	
	@ApiModelProperty(value = "Vehicle brand")
	private String brand;
	
	@ApiModelProperty(value = "Vehicle year")
	private Integer year;
	
	@ApiModelProperty(value = "Vehicle description")
	private String description;
	
	@ApiModelProperty(value = "Vehicle sold")
	private boolean sold;
	
	@ApiModelProperty(value = "Vehicle created")
	private LocalDateTime created;
	
	@ApiModelProperty(value = "Vehicle updated")
	private LocalDateTime updated;

	public Long getVehicleId() {
		return vehicleId;
	}

	public void setVehicleId(Long vehicleId) {
		this.vehicleId = vehicleId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isSold() {
		return sold;
	}

	public void setSold(boolean sold) {
		this.sold = sold;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public void setCreated(LocalDateTime created) {
		this.created = created;
	}

	public LocalDateTime getUpdated() {
		return updated;
	}

	public void setUpdated(LocalDateTime updated) {
		this.updated = updated;
	}
	
}
