package com.test.poc.vehiclecontrol.domain.request;

import com.sun.istack.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Vehicle create Request")
public class VehicleCreateRequest {

	@NotNull
	@ApiModelProperty(value = "Vehicle name")
	private String name;
	
	@NotNull
	@ApiModelProperty(value = "Vehicle brand")
	private String brand;
	
	@NotNull
	@ApiModelProperty(value = "Vehicle year")
	private Integer year;
	
	@NotNull
	@ApiModelProperty(value = "Vehicle description")
	private String description;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
