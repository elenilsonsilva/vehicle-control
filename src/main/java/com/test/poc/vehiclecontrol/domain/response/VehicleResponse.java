package com.test.poc.vehiclecontrol.domain.response;

import com.test.poc.vehiclecontrol.domain.dto.VehicleDetailRetrievalDto;

import io.swagger.annotations.ApiModel;

@ApiModel("Vehicle Response")
public class VehicleResponse {

	private VehicleDetailRetrievalDto vehicle;

	public VehicleDetailRetrievalDto getVehicle() {
		return vehicle;
	}

	public void setVehicle(VehicleDetailRetrievalDto vehicle) {
		this.vehicle = vehicle;
	}
	
}
