package com.test.poc.vehiclecontrol.domain.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import com.test.poc.vehiclecontrol.domain.dto.VehicleDetailRetrievalDto;
import com.test.poc.vehiclecontrol.domain.dto.VehicleRetrievalDto;
import com.test.poc.vehiclecontrol.domain.request.VehicleCreateRequest;
import com.test.poc.vehiclecontrol.domain.request.VehicleUpdatePartialRequest;
import com.test.poc.vehiclecontrol.domain.request.VehicleUpdateRequest;
import com.test.poc.vehiclecontrol.entity.Vehicle;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface VehicleMapper {

	@Mapping(source = "id", target = "vehicleId")
	VehicleRetrievalDto toVehicleRetrievalDto(Vehicle vehicle);
	
	List<VehicleRetrievalDto> toVehicleRetrievalDto(List<Vehicle> vehicles);

	@Mapping(source = "id", target = "vehicleId")
	VehicleDetailRetrievalDto toVehicleDetailRetrievalDto(Vehicle vehicle);

	Vehicle toEntity(VehicleCreateRequest vehicleCreateRequest);
	
	Vehicle toEntity(VehicleUpdateRequest vehicleUpdateRequest);
	
	Vehicle toEntity(VehicleUpdatePartialRequest VehicleUpdatePartialRequest);
	
}
