package com.test.poc.vehiclecontrol.domain.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Vehicle Filter Request")
public class VehicleFilterRequest {

	@ApiModelProperty(required = false, value = "Vehicle Name")
	private String name;
	
	@ApiModelProperty(required = false, value = "Vehicle Brand")
	private String brand;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}
	
}
