package com.test.poc.vehiclecontrol.domain.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Vehicle Update Patial Request")
public class VehicleUpdatePartialRequest {

	@ApiModelProperty(value = "Vehicle id")
	private Long id;
		
	@ApiModelProperty(value = "Vehicle name")
	private String name;
	
	@ApiModelProperty(value = "Vehicle brand")
	private String brand;
	
	@ApiModelProperty(value = "Vehicle year")
	private Integer year;
	
	@ApiModelProperty(value = "Vehicle description")
	private String description;
	
	@ApiModelProperty(value = "Vehicle sold")
	private boolean sold;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isSold() {
		return sold;
	}

	public void setSold(boolean sold) {
		this.sold = sold;
	}
	
}
